class Moradia:
    def __init__(self, area, endereco, numero, preco_imovel):
        self.area = area
        self.endereco = endereco
        self.numero = numero
        self.preco_imovel = preco_imovel
        self.tipo = 'Não especificado'

    def gerar_relatorio_preco(self):
        return int(self.preco_imovel / self.area)


moradia_1 = Moradia(100, 'Rua das Limeiras', 672, 250000)

print(moradia_1.__dict__)

print(moradia_1.gerar_relatorio_preco())


class Apartamento(Moradia):
    def __init__(self, area, endereco, numero, preco_imovel, preco_condominio, num_apt, num_elevadores):
        super().__init__(area, endereco, numero, preco_imovel)
        self.preco_condominio = preco_condominio
        self.andar = num_apt // 10
        self.num_apt = num_apt
        self.num_elevadores = num_elevadores
        self.tipo = 'Apartamento'

    def gerar_relatorio(self):

        return f'{self.endereco} - apt {self.num_apt} - andar: {self.andar} - elevadores: {self.num_elevadores} preco por m²: R$ {int(self.preco_imovel/self.area)}'


apt_1 = Apartamento(100, 'Rua das Limeiras', 672, 500000, 700, 110, 2)

print(apt_1.__dict__)

print(apt_1.gerar_relatorio())
